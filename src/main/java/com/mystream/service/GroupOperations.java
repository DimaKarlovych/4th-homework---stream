package com.mystream.service;

import com.mystream.model.Group;
import com.mystream.model.Student;
import com.mystream.model.Subject;
import lombok.Getter;
import java.util.*;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.*;

@Getter
public class GroupOperations {
    private List <Group> groupList = new ArrayList<>();

    public void addGroup(Group group) {
        groupList.add(group);
    }

    public Map<Integer, Double> averageGroupPoint() {
        return groupList.stream()
                .collect(toMap(
                        Group::getGroupIdentifier,
                        group -> group.getStudentList().stream()
                                .flatMap(s -> s.getMarks().values().stream())
                                .mapToInt(i -> i)
                                .average().getAsDouble()
                        )
                );
    }

    public Map<Subject, Double> averageStudentPointPerSubject() {
          return groupList.stream()
                .flatMap(group -> group.getStudentList().stream())
                 .flatMap(student -> student.getSubjectMap().entrySet().stream())
                 .collect(groupingBy
                         (Map.Entry::getKey,
                                 averagingDouble(Map.Entry::getValue)));
    }

    public Set<String> getStudentsSubject() {
        return groupList.stream()
                .flatMap(group -> group.getStudentList().stream())
                .flatMap(student -> student.getMarks().keySet().stream())
                .map(Subject::getTitle)
                .collect(Collectors.toSet());
    }

    public void isMaleGroups() {
        groupList.stream().collect(toMap(
                Group::getGroupIdentifier,
                group -> group
                .getStudentList()
                .stream()
                .map(Student::getSex)
                        .allMatch(element -> element.equals('m'))
        ))
                .forEach((groupId, oneGender) -> System.out.println("Group id:"+groupId+" Male group:"+ oneGender));
    }

    public List<Group> countSuccessfulGroup() {
         return groupList.stream()
                .filter(group -> group.getStudentList().stream()
                .filter(this::isExcellentStudent).count() > 1)
                .collect(Collectors.toList());
    }

    private boolean isExcellentStudent(Student student) {
        return student.getMarks().values().stream()
                .filter(mark -> mark > 89).count() >= 3;
    }

    public List<Student> showAllGroups() {
        return groupList
                .stream()
                .map(Group::getStudentList)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public long countUnsuccessfulGroup() {
        return groupList
                .stream()
                .filter(this::hasUnsuccessfulStudents)
                .count();
    }

    private boolean hasUnsuccessfulStudents(Group group) {
        return group
                .getStudentList()
                .stream()
                .map(student -> student.getMarks().values().stream())
                .flatMap(element -> element)
                .anyMatch(m -> m < 60);
    }

    public List<Student> showMilitaryAgeStudents() {
        return groupList
                .stream()
                .map(Group::getStudentList)
                .flatMap(Collection::stream)
                .filter(this::hasMilitaryAge)
                .collect(Collectors.toList());
    }

    private boolean hasMilitaryAge(Student student) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int birthYearMillitarryMaxThreshold = currentYear - 18;
        int birthYearMillitarryMinThreshold = currentYear - 27;

        return student.getBirth() >= birthYearMillitarryMinThreshold && student.getBirth() < birthYearMillitarryMaxThreshold
                && student.getSex() == 'm';
    }

    public List<Student> bestStudent() {
        return groupList.stream()
                .flatMap(group -> group.getStudentList().stream())
                .sorted().skip(2).limit(2).collect(Collectors.toList());
    }
}
