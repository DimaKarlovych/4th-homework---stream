package com.mystream;

import com.mystream.model.Group;
import com.mystream.model.Student;
import com.mystream.model.Subject;
import com.mystream.service.GroupOperations;

import java.util.*;


public class Main {
    public static void main(String[] args) {
        GroupOperations groupOperations = new GroupOperations();

        Subject subject1 = new Subject("Math");
        Subject subject2 = new Subject("English");
        Subject subject3 = new Subject("PT");
        Subject subject4 = new Subject("Ukrainian");

        Map <Subject, Integer> studentMap1 = new HashMap<>();

        studentMap1.put(subject1, 99);
        studentMap1.put(subject2, 85);
        studentMap1.put(subject3, 66);

        Map <Subject, Integer> studentMap2 = new HashMap<>();

        studentMap2.put(subject1, 98);
        studentMap2.put(subject2, 98);
        studentMap2.put(subject3, 98);

        Map <Subject, Integer> studentMap3 = new HashMap<>();

        studentMap3.put(subject1, 99);
        studentMap3.put(subject2, 99);
        studentMap3.put(subject3, 99);

        Map <Subject, Integer> studentMap4 = new HashMap<>();

        studentMap4.put(subject1, 100);
        studentMap4.put(subject2, 100);
        studentMap4.put(subject3, 100);

        Map<Subject, Integer> studentMap5 = new HashMap<>();

        studentMap5.put(subject1, 100);
        studentMap5.put(subject2, 100);
        studentMap5.put(subject3, 100);

        Student student1 = new Student("Karlovych Dima",'m', 1999, studentMap1);
        Student student2 = new Student("Efremov Danil",'m', 2000, studentMap2);
        Student student3 = new Student("Efimov Denis", 'm',1999, studentMap3);
        Student student4 = new Student("Top 1", 'm', 1999, studentMap4);
        Student student5 = new Student("Top 2", 'm', 1999, studentMap5);

        Group group1 = new Group(1);

        group1.addStudent(student1);
        group1.addStudent(student2);
        group1.addStudent(student3);
        group1.addStudent(student4);
        group1.addStudent(student5);

        Map<Subject, Integer> studentMap6 = new HashMap<>();

        studentMap6.put(subject1, 60);
        studentMap6.put(subject2, 70);
        studentMap6.put(subject4, 80);

        Map<Subject, Integer> studentMap7 = new HashMap<>();

        studentMap7.put(subject1, 30);
        studentMap7.put(subject2, 40);
        studentMap7.put(subject4, 50);

        Map<Subject, Integer> studentMap8 = new HashMap<>();

        studentMap8.put(subject1, 68);
        studentMap8.put(subject2, 86);
        studentMap8.put(subject4, 62);

        Student student6 = new Student("Kovalchuk Sergei",'m', 1998, studentMap6);
        Student student7 = new Student("Donsova Diana",'f', 1989, studentMap7);
        Student student8 = new Student("Molchanovskaya Irina",'f', 1999, studentMap8);

        Group group2 = new Group(2);

        group2.addStudent(student6);
        group2.addStudent(student7);
        group2.addStudent(student8);

        groupOperations.addGroup(group1);
        groupOperations.addGroup(group2);

        System.out.println(groupOperations.showAllGroups());

        System.out.println(groupOperations.averageGroupPoint());

        groupOperations.isMaleGroups();

        System.out.println(groupOperations.showMilitaryAgeStudents());

        System.out.println(groupOperations.countSuccessfulGroup());

        System.out.println(groupOperations.countUnsuccessfulGroup());

        System.out.println(groupOperations.getStudentsSubject());

        System.out.println(groupOperations.averageStudentPointPerSubject());

        System.out.println(groupOperations.bestStudent());
    }
}
