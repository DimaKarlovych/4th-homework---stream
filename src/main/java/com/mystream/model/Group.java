package com.mystream.model;

import lombok.Getter;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Group {
    private int groupIdentifier = 0;
    private List <Student> studentList = new ArrayList<>();
    private List <Student> group = new ArrayList<>();

    public Group(int groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }

    @Override
    public String toString() {
        return "Group{" +
                "studentList=" + studentList +
                '}';
    }

    public void addStudent(Student student) {
        studentList.add(student);
    }
}
