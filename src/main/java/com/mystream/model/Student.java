package com.mystream.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Map;

@AllArgsConstructor
@Getter
@ToString
public class Student implements Comparable<Student>{
    private String fio;
    private char sex;
    private int birth;
    private Map<Subject, Integer> subjectMap;

    public Map<Subject, Integer> getMarks() {
       return subjectMap;
    }

    @Override
    public int compareTo(Student student) {
        Double st1 = this.getMarks().values().stream().mapToInt(i -> i).average().getAsDouble();
        Double st2 = student.getMarks().values().stream().mapToInt(i -> i).average().getAsDouble();
        return st2.compareTo(st1);
    }
}
